package mock;

class MockDoorPanel extends DoorPanel {
    private boolean calledClose = false;

    boolean isCalledClose() {
        return calledClose;
    }

    @Override
    void close() {
        calledClose = true;
    }
}
